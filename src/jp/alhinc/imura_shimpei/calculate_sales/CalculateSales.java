package jp.alhinc.imura_shimpei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//System.out.println("ここにあるファイルを開きます→"+args[0]);

		HashMap<String,String> branchcode = new HashMap<String,String>();//支店定義ファイル
		HashMap<String,Long> branchsales = new HashMap<String,Long>();
		List<String> salesData = new ArrayList<String>();//売上ファイル


		BufferedReader br = null;
		try {
			File branch = new File(args [0],"branch.lst");
			
			if (!(branch.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			
			FileReader fr=new FileReader(branch);
			br=new BufferedReader(fr);
			
			String line;
			while((line=br.readLine())!=null) {

				String[] data=line.split(",");
				
				
				if(data.length != 2 | !(data[0].matches("^[0-9]{3}"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				branchcode.put(data[0],data[1]);
				branchsales.put(data[0],(long)0);
				}

		}catch(IOException e) {
			System.out.println("エラーが発生しました。");
		}finally {
			if(br!=null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		//ここから集計
		//ファイル読み込み
		File syuukei = new File(args[0]);
		String[] salesFiles = syuukei.list();
		for(String salesFile : salesFiles) {
			if(salesFile.matches("[0-9]{8}.rcd")) {
				salesData.add(salesFile);
			}
		}
		
		for(int i = 0; i<salesData.size() - 1; i++) {
			int mas = Integer.parseInt((salesData.get(i)).substring(0,8));
			int mas2 = Integer.parseInt((salesData.get(i+1)).substring(0,8));
			if(!(mas2 - mas ==1)) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		
		for(int i = 0; i < salesData.size(); i ++) {
			BufferedReader br2 = null;
			try{
				File file = new File(args[0],salesData.get(i));
				br2 = new BufferedReader(new FileReader(file));
				String line;
				List<String> salescode = new ArrayList<String>();
				
				while((line = br2.readLine()) !=null) {
	            	salescode.add(line);
	            	//System.out.println(salesFiles);
	            }
				
				if(!branchcode.containsKey(salescode.get(0))) {
				System.out.println(salesData.get(0) + "の支店コードが不正です");
				}
				
				if(salescode.size() !=2) {
					System.out.println(salesData.get(0) + "のフォーマットが不正です");
				}
				

	            Long rcd = Long.parseLong(salescode.get(1));//rcdの売上金額 
	            Long sale = branchsales.get(salescode.get(0));//売上集計ファイルの合計金額

	            Long lon = sale += rcd;
	            
	            if(lon >= 1000000000l) {
	            	System.out.println("合計金額が10桁を超えました");
	            	return;
	            }

	            branchsales.put(salescode.get(0), lon);

	            
            }catch(IOException e) {
            	System.out.println("エラーが発生しました");
            	
        	}finally {
        		if(br2 !=null) {
    				try {
    					br2.close();
    				}catch(IOException e) {
    					System.out.println("closeできませんでした。");
    				}
        		}
        	}
		}

        File branch = new File(args[0],"branch.out");
        PrintWriter pw = null;
        try {
        	branch.createNewFile();
            FileWriter fin = new FileWriter(branch);
           	pw = new PrintWriter(new BufferedWriter(fin));

           	for(String key : branchsales.keySet()) {
               	pw.println(key+","+branchcode.get(key)+","+branchsales.get(key));
               	}
           	
    	}catch(IOException e) {
    		System.out.println("予期せぬエラーが発生しました");
    		return;
    	}finally {
    		if(pw !=null) {
				pw.close();
				}
    	}
	}
}